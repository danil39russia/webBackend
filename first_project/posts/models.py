from django.db import models
from django import forms

# Create your models here.
    
class Service(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    text = models.CharField(max_length=500)
    price = models.IntegerField()

    def __str__(self):
        return self.name #для красоты вывода в админку

class UserReq(models.Model):
    name = models.CharField(max_length=50, default=0) #blank = false
    email = models.CharField(max_length=500, default=0)
    typeWork = models.CharField(max_length=50, default=0)
    message = models.CharField(max_length=500, default=0)

    def __str__(self):
        return self.name #для красоты вывода в админку
