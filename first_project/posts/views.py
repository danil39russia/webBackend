from django.shortcuts import render
from .models import Service
from .models import UserReq
# from string import zfill

def index(request):
    if request.method=='POST':
        name=request.POST['name']
        email=request.POST['email']
        typeWork=request.POST['type-work']
        message=request.POST['message']
        UserReq.objects.create(name=name,email=email,typeWork=typeWork,message=message)
    services = Service.objects.all()
    # services = Service.objects.filter()
    for service in services:
        #service.id = zfill(service.id, 2)
        if service.id < 9:
            service.id = '0' + str(service.id)
        service.price = '{0:,}'.format(service.price).replace(',', ' ')
    return render(request, "index.html", {'services': services})
    